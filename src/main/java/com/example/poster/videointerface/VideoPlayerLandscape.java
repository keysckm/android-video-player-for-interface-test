package com.example.poster.videointerface;

import android.os.Bundle;

/**
 * Created by KyeongMin on 2015-02-11.
 * Landscape용 VideoPlayer Activity 클래스
 */
public class VideoPlayerLandscape extends VideoPlayer {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
