package com.example.poster.videointerface;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by KyeongMin on 2015-01-28.
 * 동영상 리스트 어댑터 클래스
 */
public class VideoItemListAdapter extends BaseAdapter {
    private Context mContext = null;
    private ArrayList<VideoData> mList = null;

    public VideoItemListAdapter(Context context, ArrayList<VideoData> list) {
        mContext = context;
        mList = list;
    }

    public void clear() {
        mList.clear();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VideoItemView itemView;
        if(convertView == null){
            itemView = new VideoItemView(mContext);
        }
        else
        {
            itemView = (VideoItemView)convertView;
        }

        // video item view의 각 항목 설정
        VideoData d = mList.get(position);
        itemView.setThumbnail(d.VideoThumbnail);    // 썸네일
        itemView.setTitle(d.VideoTitle);            // 타이틀

        return itemView;
    }
}
