package com.example.poster.videointerface;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;


public class MainActivity extends ActionBarActivity {

    // 외부 클래스에서 접근할 수 있는 전역 activity 변수
    static Activity global_mainactivity;    // main activity
    static VideoData global_nowplaying;     // 현재 재생중인 동영상

    // 동영상 목록 리스트 뷰
    static ArrayList<VideoData> mList = null;
    private static ListView mVideoListView = null;
    private static VideoItemListAdapter mAdapter = null;  // 어뎁터

    // cursor
    private Cursor VideoCursor = null;

    // 마지막으로 backkey를 누른 시각
    private long mBackkeyPushTime = 0;

    // Menu item
    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        global_mainactivity = this;
        mVideoListView = (ListView)findViewById(R.id.listview_video);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(mList == null) {     // 앱이 처음 실행되었음.
            new DoRefreshTask(this).execute();
        }
        else {                  // 액티비티가 죽었다 살아남.
            mVideoListView.setAdapter(mAdapter);                    // 리스트뷰 연결
            mVideoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {   // 콜백
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    VideoData d = mList.get(position);
                    callVideoPlayerActivity(d);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(VideoCursor != null && !VideoCursor.isClosed()) {
            Log.i("Cursor", "Close Video Cursor");
            VideoCursor.close();
            VideoCursor = null;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        switch(keyCode)
        {
            case KeyEvent.KEYCODE_BACK:
                long now = System.currentTimeMillis();
                Log.i("backkey", "offset " + (now - mBackkeyPushTime));
                if(now - mBackkeyPushTime < 2500) {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid()); // 프로세스를 죽인다.
                }
                else {
                    Toast.makeText(getApplicationContext(), "한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
                    mBackkeyPushTime = now;
                }
                return true;
            default:
                return super.onKeyDown(keyCode, e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch(id)
        {
            case R.id.action_refresh:
                new DoRefreshTask(this).execute();
                break;

            case R.id.action_settings:
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    /**
     * 새로고침 버튼에 새로고침 아이콘이 회전하는 애니메이션을 세팅한다.
     */
    public void setRefreshAnimation() {
        MenuItem item = mMenu.findItem(R.id.action_refresh);
        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView)inflater.inflate(R.layout.iv_refresh, null);
        Animation anim_rotation = AnimationUtils.loadAnimation(this, R.anim.rotate_refresh);
        anim_rotation.setRepeatCount(Animation.INFINITE);
        item.setActionView(iv);
        iv.startAnimation(anim_rotation);
    }

    /**
     * 새로고침 버튼에 세팅된 애니메이션을 해제한다.
     */
    public void resetRefreshAnimation() {
        MenuItem m = mMenu.findItem(R.id.action_refresh);
        if(m.getActionView() != null) {
            m.getActionView().clearAnimation();
            m.setActionView(null);
        }   // animation reset
    }

    /**
     * 동영상 플레이어 액티비티를 호출, 전환합니다.
     * @param data 동영상의 경로
     */
    private void callVideoPlayerActivity(VideoData data){
        global_nowplaying = data;
        Intent intent = null;
        if(data.VideoWidth <= data.VideoHeight)                     // 동영상의 가로세로 길이를 비교하여
            intent = new Intent(this, VideoPlayer.class);           // 서로 다른 VideoPlayer 클래스를 넘깁니다.
        else
            intent = new Intent(this, VideoPlayerLandscape.class);

        startActivity(intent);
    }

    /**
     * MediaStore에서 동영상 목록 정보를 가져온다.
     * @return 동영상 정보 리스트
     */
    @SuppressWarnings("deprecation")
    public ArrayList<VideoData> getVideoList()
    {
        // 동영상 리스트가 담길 ArrayList
        ArrayList<VideoData> list = new ArrayList<VideoData>();

        Log.i("getVideoList", "search video");
        String[] column = {                           // Media Information Column List
                MediaStore.Video.Media._ID,         // index
                MediaStore.Video.Media.DATA,        // path
                MediaStore.Video.Media.SIZE ,       // file size
                MediaStore.Video.Media.TITLE,       // title
                MediaStore.Video.Media.DURATION,    // 영상 길이
                MediaStore.Video.Media.DATE_ADDED,  // 추가 시각
                MediaStore.Video.Media.WIDTH,       // 해상도(가로)
                MediaStore.Video.Media.HEIGHT       // 해상도(새로)
        };
        // DB에서 동영상 정보를 획득한다.
        VideoCursor = this.managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, column, null, null, null);

        if(VideoCursor != null)
        {
            int VideoCnt = VideoCursor.getCount();  // 쿼리 결과 갯수 반영
            Log.i("VideoSearch", "Video Count: " + VideoCnt);
            if(VideoCnt == 0) { return list; }

            // video column index 가져오기
            int id          = VideoCursor.getColumnIndex(MediaStore.Video.Media._ID);
            int size	    = VideoCursor.getColumnIndex(MediaStore.Video.Media.SIZE);
            int title	    = VideoCursor.getColumnIndex(MediaStore.Video.Media.TITLE);
            int data	    = VideoCursor.getColumnIndex(MediaStore.Video.Media.DATA);
            int duration	= VideoCursor.getColumnIndex(MediaStore.Video.Media.DURATION);
            int date		= VideoCursor.getColumnIndex(MediaStore.Video.Media.DATE_ADDED);
            int width       = VideoCursor.getColumnIndex(MediaStore.Video.Media.WIDTH);
            int height      = VideoCursor.getColumnIndex(MediaStore.Video.Media.HEIGHT);

            VideoCursor.moveToFirst();  // 첫번째 결과로 이동
            // 쿼리 결과를 탐색하며 리스트 작성
            do{
                VideoData videoData = new VideoData();
                videoData.VideoId = VideoCursor.getLong(id);
                videoData.VideoFilePath = VideoCursor.getString(data);
                videoData.VideoTitle = VideoCursor.getString(title);
                videoData.VideoSize = VideoCursor.getLong(size);
                videoData.VideoDuration = VideoCursor.getLong(duration);
                videoData.VideoWidth = VideoCursor.getInt(width);
                videoData.VideoHeight = VideoCursor.getInt(height);
                videoData.VideoAddDate = (new Date(VideoCursor.getLong(date)*1000)).toString();
                // videoData.VideoThumbnail = getVideoThumnail(videoData.VideoFilePath);

                // 리스트에 저장
                list.add(videoData);
            }while(VideoCursor.moveToNext());
        }

        return list;
    }

    /**
     * 동영상의 썸네일을 가져옴
     * @param path  동영상의 path
     * @return      썸네일 이미지에 대한 Bitmap 객체
     */
    public Bitmap getVideoThumnail(String path)
    {
        Bitmap bmThumbnail = null;
        // 정사각형 모양의 작은 크기의 섬네일을 가져온다.
        bmThumbnail = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
        return bmThumbnail;
    }

    /**
     * 썸네일을 수집하는 AsyncTask
     */
    private class DoThumbnailTask extends AsyncTask<ArrayList<VideoData>, Void, Void> {

        DoThumbnailTask() {
            Log.i("Task", "Called DoThumbnailTask()");
        }

        @Override
        protected Void doInBackground(ArrayList<VideoData>... params) {
            for (ArrayList<VideoData> l : params) {
                for(VideoData d : l) {
                    d.VideoThumbnail = getVideoThumnail(d.VideoFilePath);
                    publishProgress();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... progress) {
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 새로고침을 수행하는 AsyncTask
     */
    private class DoRefreshTask extends AsyncTask<Void, Void, ArrayList<VideoData>> {

        private Context c;

        DoRefreshTask(Context c) {
            this.c = c;
            Log.i("Task", "Called DoRefreshTask()");
        }

        @Override
        protected void onPreExecute() {
            setRefreshAnimation();
        }

        @Override
        protected ArrayList<VideoData> doInBackground(Void... params) {
            return getVideoList();
        }

        @Override
        protected void onPostExecute(ArrayList<VideoData> result) {
            mList = result;
            mAdapter = new VideoItemListAdapter(c, mList);       // 어뎁터 연결
            mVideoListView.setAdapter(mAdapter);                 // 리스트뷰 연결
            mVideoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {   // 콜백
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    VideoData d = mList.get(position);
                    callVideoPlayerActivity(d);
                }
            });
            Log.i("Task", "DoRefreshTask finished...");
            resetRefreshAnimation();
            new DoThumbnailTask().execute(mList);
        }
    }
}
