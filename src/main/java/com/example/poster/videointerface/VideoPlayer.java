package com.example.poster.videointerface;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by KyeongMin on 2015-01-28.
 * 비디오를 재생하는 액티비티
 * 이 클래스는 Activity를 Portrait로 출력하기 위한 클래스입니다.
 * Landscape용 클래스는 VideoPlayer 클래스를 상속한 VideoPlayerLandscape 클래스를 이용합니다.
 * 단 그 외에, VideoPlayerLandscape 클래스와 VideoPlayer클래스의 차이점은 존재하지 않습니다.
 */
public class VideoPlayer extends Activity {

    public enum VideoState {
        play,
        pause,
        stop,
    };

    public enum TouchState {
        down,
        drag,
        none,
    }

    protected long mVideoId = -1;

    // 재생 될 동영상의 path
    protected String mVideoPath = null;

    // 재생 될 동영상의 title
    protected String mVideoTitle = null;

    // 설정값 백업
    protected WindowManager.LayoutParams mOldParam;
    protected boolean mOldAutobrightness;

    final int ff_rw_offset = 5000;      // 빨리감기/되감기 offset

    // Video Play 관련
    SurfaceHolder   mShVideo = null;
    MediaPlayer     mPlayer = null;

    // 컨트롤러 뷰 관련
    LinearLayout    mControllerLayout = null;       // 터치하는 부분에 나타나는 컨트롤러
    LinearLayout    mBottomControllerLayout = null; // 하단 부분의 컨트롤러
    SurfaceView     mSurfaceView = null;            // surface view
    Button          mBtnPlay = null;                // 재생/일시정지 버튼
    Button          mBtnNext = null;                // 다음 트랙으로
    Button          mBtnPrev = null;                // 이전 트랙으로
    SeekBar         mSeekCtrl = null;               // 현재 재생 상태를 나타내는 seek bar
    TextView        mTxtNowPosition = null;         // 현재 재생 시간
    TextView        mTxtVideoDuration = null;       // 동영상의 총 길이
    boolean         mIsShowController = false;      // 컨트롤러의 표시상태 표시
    boolean         mIsUserCall = false;            // 사용자가 상호작용하여 컨트롤러가 출력되었는지 여부
    ScreenCtrl.Size mControllerSize = null;         // 컨트롤러의 크기
    ScreenCtrl.Size mBottomControllerSize = null;   // 하단 컨트롤러의 크기

    // 화면 크기
    ScreenCtrl.Size mScreenSize = null;             // 영상이 출력되는 레이아웃 사이즈
    Rect            mBrightnessRect = null;         // 밝기 조절 영역
    Rect            mVolumeRect = null;             // 볼륨 조절 영역

    // 재생 상태 관련
    VideoState      mState = VideoState.stop;       // 현재 재생 상태
    VideoPlayback   mPlayback = null;               // MediaPlayer <-> SeekBar 연동

    // 기타 UI 요소
    ActionBar       mActionBar = null;              // actionbar
    TouchState      mTouchState = TouchState.none;
    Bundle          mSavedBundle = null;

    // callback 메소드
    Handler mHideCtrllerHandler = new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            hideController();
        }
    };
    View.OnClickListener mPlayListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            playProc();
        }};
    View.OnClickListener mPauseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) { pauseProc();
        }};
    View.OnClickListener mNextListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            next();
        }
    };
    View.OnClickListener mPrevListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            prev();
        }
    };

    SurfaceHolder.Callback mSurfaceCallback = new SurfaceHolder.Callback(){
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            Log.i("Surface", "Called surfaceCreated()!");

            if(mPlayer == null)
                loadVideo(mVideoPath);  // 동영상 로드 및 재생
            else {
                mPlayer.setDisplay(holder);
                if(mState == VideoState.play)           playProc();
                else if(mState == VideoState.pause)     pauseProc();
            }
        }
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {}
    };

    // thread 필드
    class DelayHideControllerTask extends Thread {
        private boolean misCanceled = false;
        private long mTimestamp = System.currentTimeMillis();

        @Override
        public void run() {         // 초기화 후 3초 뒤에 컨트롤러를 비가시 상태로 전환
            try {
                Log.i("Thread", "DelayHideController Thread Start: " + mTimestamp);
                Thread.sleep(3000);
                if(!misCanceled)
                    mHideCtrllerHandler.sendEmptyMessage(0);
                Log.i("Thread", "DelayHideController Thread End: " + mTimestamp);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void cancel() {
            Log.i("Thread", "DelayHideController Thread Cancel: " + mTimestamp);
            misCanceled = true;
        }
    }
    DelayHideControllerTask mControllerHideTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);      // overlay actionbar request
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);                // 상단 네비게이션 바 숨기기
        setContentView(R.layout.activity_player);

        Log.i("Player", "Created Activity!!");

        // 스크린 설정값을 보관한다.
        mOldParam = getWindow().getAttributes();
        mOldAutobrightness = ScreenCtrl.isAutoBrightness(this);

        // 화면 켜짐 유지
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if(savedInstanceState != null)
            mSavedBundle = savedInstanceState.getBundle("PLAYERSTATE");

        // 동영상 정보를 가져온다.
        mVideoId = MainActivity.global_nowplaying.VideoId;
        mVideoPath = MainActivity.global_nowplaying.VideoFilePath;
        mVideoTitle = MainActivity.global_nowplaying.VideoTitle;

        init(true); // interface 및 ui 초기화

        // Surface View 로드
        mShVideo = mSurfaceView.getHolder();    // surface view에 대한 surface holder
        mShVideo.addCallback(mSurfaceCallback); // surfaceview callback
    }

    private void init(boolean isCreated) {
        mActionBar = getActionBar();
        try {
            mActionBar.setDisplayShowHomeEnabled(false);    // 액션바 아이콘 숨기기
            mActionBar.setTitle(mVideoTitle);               // 액션바 타이틀 설정 -> 파일이름
            mActionBar.show();

            mIsUserCall = false;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if(isCreated) {
                // 다른 컨트롤러와 함께 보여졌다 숨겨졌다 함.
                // 뷰 초기화
                mControllerLayout = (LinearLayout) findViewById(R.id.layout_controller);
                mBottomControllerLayout = (LinearLayout) findViewById(R.id.layout_bottom_controller);
                mSurfaceView = (SurfaceView) findViewById(R.id.sv_player);
                mBtnPlay = (Button) findViewById(R.id.btn_play);
                mBtnNext = (Button) findViewById(R.id.btn_next);
                mBtnPrev = (Button) findViewById(R.id.btn_prev);
                mSeekCtrl = (SeekBar) findViewById(R.id.seek_ctrl);
                mTxtNowPosition = (TextView) findViewById(R.id.txt_now_playtime);
                mTxtVideoDuration = (TextView) findViewById(R.id.txt_video_duration);

                // 각 뷰에 대한 콜백 매핑
                mBtnPlay.setOnClickListener(mPlayListener);
                mBtnNext.setOnClickListener(mNextListener);
                mBtnPrev.setOnClickListener(mPrevListener);
            }
            hideControllerDelay();
        }
    }

    @Override
    public void onPause() {
        getWindow().setAttributes(mOldParam);                       // 스크린 설정값 복원
        ScreenCtrl.setAutoBrightness(this, mOldAutobrightness);     // 자동 밝기 설정 복원
        VideoState s = mState;      // 재생 상태 백업
        if(mPlayer != null) {
            pauseProc();            // 일시정지
        }
        mState = s;                 // 백업 적용
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        // 뷰 크기 계산
        mScreenSize = ScreenCtrl.getRealScreenSize();

        // virtual nav bar 높이를 보정한다. (실제 스크린 사이즈에서 제외함)
        RelativeLayout calc_layout = (RelativeLayout)findViewById(R.id.layout_calcsize);
        mScreenSize.width -= (mScreenSize.width - calc_layout.getWidth());
        mScreenSize.height -= (mScreenSize.height - calc_layout.getHeight());

        // 밝기조절 영역을 구한다.
        mBrightnessRect = new Rect();
        mBrightnessRect.set(0, 0, mScreenSize.width/2, mScreenSize.height);

        // 음량조절 영역을 구한다.
        mVolumeRect = new Rect();
        mVolumeRect.set(mScreenSize.width/2, 0, mScreenSize.width, mScreenSize.height);

        // 컨트롤러 영역 설정
        mControllerLayout.setX(mScreenSize.width);
        mControllerSize = new ScreenCtrl.Size(mControllerLayout.getWidth(), mControllerLayout.getHeight());
        mBottomControllerSize = new ScreenCtrl.Size(mScreenSize.width, (int)ScreenCtrl.dp2px(32));
        mBottomControllerLayout.setY(mScreenSize.height - mBottomControllerSize.height);
        mIsUserCall = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBundle("PLAYERSTATE", saveState());
    }

    public enum SettingTarg {
        brightness,
        volume,
        seek,
        none,
    }
    private Point   mTouch_pt = null;                       // 터치 기준 좌표
    private boolean mIsDraw = false;                        // drag 하였나?
    private SettingTarg targState = SettingTarg.none;       // setting 상태
    private int drag_level = 0;                             // 드래그가 지속되고 있을 때 절대적인 변화기준
    private int before_seek_playtime = 0;                   // 좌우 드래그로 탐색을 할 때 기준이 되는 재생위치
    private boolean before_playing = false;                 // 좌우 드래그로 탐색을 하기 전에 재생중이였는가
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        final int settings_level = 15;                          // 최대 setting 값 (0~15)
        final int level_pixel = (int)((mScreenSize.height/1.5) / settings_level);   // level당 pixel변화량 기준, 전체 영역의 2/3영역만큼 드래그 했을때 최대치가 되도록 설정한다.
        final int level_brightness = 255/settings_level;        // level당 brightenss 변화량(0~255)
        final float level_brightness_window = 0.067f;           // level당 brightness 변화량(0.0f~1.0f)
        final int need_x_change_amount = 100;                   // 탐색모드 진입을 위해 움직여야 하는 x좌표 변화량 기준

        switch(e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mTouch_pt = new Point((int)e.getX(), (int)e.getY());                    // 최초 터치 좌표 백업
                Log.i("touch_event", "down / x: " + e.getX() + " / y: " + e.getY());
                break;

            case MotionEvent.ACTION_MOVE:
                int now_drag_level = 0; // 변화량 레벨 초기화

                // 변화량을 계산하여 일정 거리 이상 드래그 하지 않으면 드래그 이벤트를 무시
                int x_pixel_change_amount = (int)e.getX() - mTouch_pt.x;
                int y_pixel_change_amount = (int)e.getY() - mTouch_pt.y;
                now_drag_level = (int) (y_pixel_change_amount / level_pixel); // y좌표 변화량을 level 값으로 치환.

                if(mIsDraw == false) {  // 드래그가 시작될 때 한번만 작동해야 하는 로직, 탐색을 할지 밝기를 조절할지 음량을 조절할지 결정함
                    if (x_pixel_change_amount >= need_x_change_amount || x_pixel_change_amount <= -need_x_change_amount) {   // x좌표 변화량이 10px 이상일 때
                        targState = SettingTarg.seek;                               // 드래그 상태를 동영상 위치 탐색 모드로 함
                        before_seek_playtime = mPlayer.getCurrentPosition();        // 기준이 되는 재생시점을 기억
                        if(mPlayer.isPlaying()) { pauseProc(); before_playing = true; }   // 일시정지 -> 탐색 -> 탐색 -> ... -> 재생
                    } else if (y_pixel_change_amount / level_pixel != 0) {          // x좌표 변화량이 기준치 미만이면서 y좌표가 기준치 이상 드래그됨
                        if(mBrightnessRect.contains(mTouch_pt.x, mTouch_pt.y))  {   // 터치 시작 좌표가 밝기 조절 영역 좌표 안에 있음
                            targState = SettingTarg.brightness;
                            if(ScreenCtrl.isAutoBrightness(this)) ScreenCtrl.setAutoBrightness(this, false);    // brightness auto 모드라면 manual로 전환
                        } // 밝기 조절 상태
                        else if(mVolumeRect.contains(mTouch_pt.x, mTouch_pt.y)) { targState = SettingTarg.volume; }     // 터치 시작 좌표가 음량 조절 영역 좌표 안에 있음, 음량 조절 상태
                    }
                    else {      // 드래그 무시
                        break;
                    }
                }

                if(now_drag_level == 0 && targState != SettingTarg.seek)    // 탐색 모드가 아니면서 level 변화량이 없으면 드래그 무시
                    break;

                mIsDraw = true; // draw 상태 설정 -> 컨트롤러 레이아웃 관련 동작을 무시하게 됨 (UI Visible 이라던가)
                Log.i("touch_event", "SettingState: " + targState.toString());
                Log.i("touch_event", "drag / drag_level: " + drag_level + " / now_drag_level: " + now_drag_level);
                Log.i("touch_event", "x_pixel_change_amount: " + x_pixel_change_amount);

                if(now_drag_level != drag_level || targState == SettingTarg.seek) {      // level 변화량이 있거나 탐색인 경우
                    switch (targState) {
                        case seek:  // 탐색 모드, 10px당 1초 변화
                            int offset = x_pixel_change_amount / need_x_change_amount;
                            Log.i("touch_event", "before_seek_playtime: " +  before_seek_playtime + " / change_amount: " + offset);
                            seekTo(before_seek_playtime + (offset*1000));
                            break;

                        case brightness:
                            int screen_brightness = ScreenCtrl.getScreenBrightness(this);                       // 현재 screen brightness 값을 구한다.
                            float window_brightness = ScreenCtrl.getWindowBrightness(getWindow());              // 현재 window brightness 값을 구한다.
                            Log.i("touch_event", "drag / screen_brightness: " + screen_brightness + " / window_brightness: " + window_brightness);
                            float brightness;
                            /**
                             * brightness 값은 activity(window) brightness, screen(global) brightness 값이 존재함.
                             * activity brightness 값은 해당 app에만 적용되는 밝기이며 screen brightness 값은 안드로이드 global 밝기
                             * 앱 혹은 activity에서 최초로 activity의 brightness를 얻으려 하면 BRIGHTNESS_OVERRIDE_NONE(-1)값을 리턴하게 됨.
                             * 현재 밝기값이 screen global 밝기 설정을 따르고 있다는 의미이며 screen global 밝기 설정을 가져와서 activity brightness 설정값에 맞도록 치환한 후
                             * setting 해 줘야 함
                             **** screen(global) brightness value range = 0 ~ 255 // activity(window) brightness value range = 0.0f ~ 1.0f
                             *  -> screel(global) brightness val / 255 = activity(window)brightness value
                             */
                            if (now_drag_level < drag_level) {  // 음수 변화(화면의 위로 드래그함)
                                if(window_brightness == WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE)        // activity(window) brightness가 설정되지 않았음
                                    brightness = (screen_brightness / 255) + level_brightness_window;               // screen brightness를 이용해서 설정한다.
                                else  brightness = window_brightness + level_brightness_window;
                            }
                            else {                              // 양수 변화(화면의 아래로 드래그함)
                                if(window_brightness == WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE)
                                    brightness = (screen_brightness / 255) - level_brightness_window;
                                else brightness = window_brightness - level_brightness_window;
                            }
                            Log.i("touch_event", "drag / targ_brightness: " + brightness);
                            ScreenCtrl.setWindowBrightness(getWindow(), brightness);    // activity(window) brightness 설정
                            break;

                        case volume:
                            if (now_drag_level < drag_level)    // 음수 변화(화면의 위로 드래그함)
                                SoundCtrl.volumeUp(this);
                            else                                // 양수 변화(화면의 아래로 드래그함)
                                SoundCtrl.volumeDown(this);
                            break;
                        case none:
                            break;
                    }
                }
                drag_level = now_drag_level;    // 변화량의 연속성을 위해 현재 변화량 값을 보존
                break;

            case MotionEvent.ACTION_UP:
                if(!mIsDraw) {    // drag 상태였다면 showController 메소드 호출 안함
                    if(!mIsShowController) {    // UI출력 x
                        showController(e.getX(), e.getY()); // 출력
                    }
                    else
                        hideController();   // 숨김
                }else if(mIsDraw && targState == SettingTarg.seek) {
                    if(before_playing)
                        playProc();
                }
                before_playing = false;
                targState = SettingTarg.none;   // 상태 초기화
                drag_level = 0;
                mIsDraw = false;
                break;

            default:
                return false;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        switch(keyCode)
        {
            case KeyEvent.KEYCODE_BACK: // 뒤로가기 키를 누르면 정지하고 액티비티를 종료한다.
                stop();
                break;

            default:
                return super.onKeyDown(keyCode, e);
        }
        return true;
    }

    /**
     * Activity가 비활성화 될 때 현재 재생 상태를 저장합니다.
     * @return 현재 재생 상태가 저장된 Bundle
     */
    protected Bundle saveState() {
        Bundle b = new Bundle();
        try {
            b.putInt("position", mPlayer.getCurrentPosition());
            Log.i("Save State", "pos: " + mPlayer.getCurrentPosition());
        } catch(Exception e) {}
        finally {
            b.putBoolean("isplay", mState == VideoState.play ? true : false);

            Log.i("Save State", "isPlaying: " + (mState == VideoState.play ? "true" : "false"));
        }

        return b;
    }

    /**
     * 동영상 재생을 일괄적으로 처리합니다.
     * 동영상을 재생하면서 재생버튼을 일시정지 버튼으로
     * 재생버튼의 리스너를 일시정지 리스너로 변경합니다.
     */
    public void playProc() {
        try {
            play();
            mPlayback = new VideoPlayback(mPlayer, mSeekCtrl, mTxtNowPosition, mTxtVideoDuration);
            mBtnPlay.setBackgroundResource(R.drawable.pause);   // 일시정지 아이콘으로 변경
            mBtnPlay.setOnClickListener(mPauseListener);        // 리스너를 일시정지로 변경
            hideControllerDelay();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 동영상 일시정지를 일괄적으로 처리합니다.
     */
    public void pauseProc() {
        try {
            pause();
            mPlayback.release();
            mBtnPlay.setBackgroundResource(R.drawable.play);   // 재생 아이콘으로 변경
            mBtnPlay.setOnClickListener(mPlayListener);        // 리스너를 재생으로 변경
            hideControllerDelay();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 다음 동영상을 재생합니다.
     */
    VideoData next_video = null;    // 다음번에 재생될 동영상의 정보
    public void next() {
        try {
            for (int i = 0; i < MainActivity.mList.size(); i++) {
                if (MainActivity.mList.get(i).VideoId == mVideoId) {
                    next_video = MainActivity.mList.get(i + 1);
                    break;
                }
            }   // 다음 동영상을 찾는다.
        } catch (Exception e) { // 다음 동영상이 없음
            stop();             // activity 종료
            return;
        }

        if (mPlayer.isPlaying())    // 만약 동영상이 재생중이라면 재생을 정지한다.
            mPlayer.stop();

        mPlayer.reset();                        // MediaPlayer Reset
        mVideoId = next_video.VideoId;
        mVideoPath = next_video.VideoFilePath;
        mVideoTitle = next_video.VideoTitle;
        MainActivity.global_nowplaying = next_video;
        init(false);                            // ui 초기화
        loadVideo(next_video.VideoFilePath);    // video load
    }

    /**
     * 이전 동영상을 재생합니다.
     */
    public void prev() {
        try {
            for (int i = 0; i < MainActivity.mList.size(); i++) {
                if (MainActivity.mList.get(i).VideoId == mVideoId) {
                    next_video = MainActivity.mList.get(i - 1);
                    break;
                }
            }   // 다음 동영상을 찾는다.
        } catch (Exception e) { // 다음 동영상이 없음
            stop();             // activity 종료
            return;
        }

        if(mPlayer.isPlaying())                 // 재생중이라면
            mPlayer.stop();                     // 정지한다.

        mPlayer.reset();                        // MediaPlayer Reset
        mVideoId = next_video.VideoId;
        mVideoPath = next_video.VideoFilePath;
        mVideoTitle = next_video.VideoTitle;
        MainActivity.global_nowplaying = next_video;
        init(false);                            // ui 초기화
        loadVideo(next_video.VideoFilePath);    // video load
    }

    /**
     * 동영상을 로드하고 재생합니다.
     * @param path 재생할 동영상 경로
     */
    public void loadVideo(String path)
    {
        try{
            if(mPlayer == null)
                mPlayer = new MediaPlayer();

            if(mPlayback != null) {
                mPlayback.release();
                mPlayback = null;
            }

            mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Log.e("MediaPlayer", "!!!!! onError--->   what:" + what + "    extra:" + extra + " !!!!!");
                    Toast.makeText(getApplicationContext(), "동영상 재생에 실패했습니다.", Toast.LENGTH_SHORT).show();
                    stop();
                    return false;
                }
            });
            mPlayer.setDataSource(path);        // 데이터 소스(동영상) 연결
            mPlayer.setDisplay(mShVideo);       // 디스플레이 연결
            mPlayer.prepareAsync();             // 비동기 동영상 준비
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {    // 비동기 작업이 완료되었을 때
                @Override
                public void onPrepared(MediaPlayer mp) {           // 동영상이 모두 로드되었을 때 호출되는 콜백 등록
                    boolean isPlay = true;  // 로드가 완료되면 재생한다.
                    try {
                        if (mSavedBundle != null) {                         // 저장되어 있는 재생정보 bundle 데이터가 존재한다면 -> Activity가 비활성화 되었다 활성화됨
                            int pos = mSavedBundle.getInt("position");      // 재생중이던 위치 획득
                            isPlay = mSavedBundle.getBoolean("isplay");     // 재생 여부 획득

                            seekTo(pos);    // 탐색
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if(isPlay)  playProc();     // 재생
                        else        pauseProc();    // 일시정지
                    }
                }
            });
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {    // 동영상이 모두 재생되었을 때 호출되는 콜백 등록
                @Override
                public void onCompletion(MediaPlayer mp) {
                    next();
                }
            });
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 동영상을 재생합니다.
     */
    protected void play() {
        if(mPlayer != null && !mPlayer.isPlaying()) {
            mPlayer.start();
            mState = VideoState.play;       // 상태 설정 -> play
        }
    }

    /**
     * 동영상을 일시정지합니다.
     */
    protected void pause() {
        if(mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();                // (재생 상태일 때는 일시정지 아이콘이였음)
            mState = VideoState.pause;      // 상태 설정 -> pause
        }
    }

    /**
     * 동영상 재생을 중지하고 액티비티를 종료합니다.
     */
    protected void stop() {
        try {
            if(mPlayback != null)
                mPlayback.release();

            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            MainActivity.global_nowplaying = null;
            finish();
        }
    }

    /**
     * 재생 위치를 이동한다.
     * @param time 이동할 재생 위치, ms
     */
    protected void seekTo(int time) {
        mPlayer.seekTo(time);
        mSeekCtrl.setProgress(time);
        mTxtNowPosition.setText(mPlayback.pos2date(time));
        Log.i("seek", "seekTo args(time): " + time + " / mediaplayer_getCurrentPosition(): " + mPlayer.getCurrentPosition());
    }

    /**
     * 컨트롤러를 출력한다.
     * @param x 컨트롤러가 출력될 x좌표
     * @param y 컨트롤러가 출력될 y좌표
     */
    private void showController(float x, float y){
        final int top_limit = mActionBar.getHeight();
        mIsUserCall = true;

        if(!mActionBar.isShowing())
            mActionBar.show();                  // 액션바 표시

        // 컨트롤러 레이아웃의 좌상단 좌표를 계산한다.
        // 컨트롤러 사이즈는 레이아웃 파일에 정의되어 있음. ScreenCtrl.dp2px 메소드를 이용해 pixel 값으로 변환
        int ctrl_pos_x = 0;
        int ctrl_pos_y = 0;
        int ctrl_bot_pos_y = 0;
        if(mControllerSize != null) {
            ctrl_pos_x = (int) x - mControllerSize.width / 2;
            ctrl_pos_y = (int) y - mControllerSize.height / 2;
        }

        // 하단 컨트롤러 레이아웃의 높이 계산
        // 하단 컨트롤러의 너비는 화면 너비폭으로 고정되어 있음
        if(mBottomControllerSize != null) { ctrl_bot_pos_y = mScreenSize.height - mBottomControllerSize.height; }

        // 출력될 위치가 실제 화면을 넘어가는지 검사
        if (ctrl_pos_x < 0)
            ctrl_pos_x = 0;
        else if (ctrl_pos_x + mControllerSize.width > mScreenSize.width)
            ctrl_pos_x = mScreenSize.width - mControllerSize.width;
        if (ctrl_pos_y + mControllerSize.height > ctrl_bot_pos_y)
            ctrl_pos_y = ctrl_bot_pos_y - mControllerSize.height;
        else if (ctrl_pos_y < top_limit)
            ctrl_pos_y = top_limit;

        Log.i("event_pos", "" + x + "/" + y);
        Log.i("ctrl_size", "" + mControllerSize.width + "/" + mControllerSize.height);
        Log.i("ctrl_pos", "" + ctrl_pos_x + "/" + ctrl_pos_y);

        // 컨트롤러 레이아웃의 xy 좌표 설정
        mControllerLayout.setX(ctrl_pos_x);
        mControllerLayout.setY(ctrl_pos_y);
        // 레이아웃 가시화
        mControllerLayout.setVisibility(View.VISIBLE);
        // 하단 컨트롤러 가시화
        mBottomControllerLayout.setX(0);
        mBottomControllerLayout.setY(ctrl_bot_pos_y);
        mBottomControllerLayout.setVisibility(View.VISIBLE);

        mControllerLayout.bringToFront();   // 컨트롤러 레이어를 모든 프레임 레이아웃의 뷰들보다 가장 앞으로 당겨옴
        mControllerLayout.invalidate();     // 컨트롤러를 다시 그린다.

        mIsShowController = true;

        hideControllerDelay();
    }

    /**
     * 모든 컨트롤러를 숨깁니다.
     */
    private void hideController() {
        mActionBar.hide();                  // 액션바 숨기기

        // 하단바 숨기기
        // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LOW_PROFILE);

        // 레이아웃 비가시화
        mControllerLayout.setVisibility(View.INVISIBLE);
        mBottomControllerLayout.setVisibility(View.INVISIBLE);


        mIsShowController = false;
    }

    /**
     * 모든 컨트롤러를 3초 뒤에 숨깁니다.
     */
    private void hideControllerDelay() {
        if(mControllerHideTask != null) {
            mControllerHideTask.cancel();
            mControllerHideTask = null;
        }

        mControllerHideTask = new DelayHideControllerTask();  // 3초 뒤에 컨트롤러 비가시화
        mControllerHideTask.start();
    }

     /**
     * Created by KyeongMin on 2015-01-28.
     * MediaPlayer의 재생시간과 SeekBar의 seek를 동기화 하기 위한 Playback Scheduler
     */
     private class VideoPlayback {

        MediaPlayer mPlayer;        // 플레이어
        SeekBar     mSeekBar;       // 탐색바

        TextView    mTxtNowPos = null;        // 현재 재생 시간
        TextView    mTxtVideoDuration = null; // 동영상 길이

        int         mVideoDuration;
        int         mCurrentPosition;

        ScheduledFuture mFuture;    // 스케쥴러

        // 시크바 리스너
        SeekBar.OnSeekBarChangeListener mSeekbarListener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser && mPlayer != null) {    // 사용자의 조작에 의한 변경인 경우에만 플레이어의 재생위치를 변경한다.
                    mPlayer.seekTo(progress);

                    if (mTxtNowPos != null)
                        mTxtNowPos.setText(pos2date(mCurrentPosition));
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mControllerHideTask.cancel();
                mControllerHideTask = null;
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                hideControllerDelay();
            }
        };

        Handler mMoniteringHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                monitor();
            }
        };

        public VideoPlayback(MediaPlayer player, SeekBar seek) {
            mPlayer = player;
            mSeekBar = seek;

            init();
        }

        public VideoPlayback(MediaPlayer player, SeekBar seek, TextView nowpos, TextView duration) {
            mPlayer = player;
            mSeekBar = seek;
            mTxtNowPos = nowpos;
            mTxtVideoDuration = duration;

            init();
        }

         /**
          * 동영상의 재생시간에 맞춰 seekbar와 재생시간을 동기화합니다.
          */
        void monitor() {
            try {
                if (mPlayer.isPlaying()) // 재생중인 경우
                {
                    mCurrentPosition = mPlayer.getCurrentPosition();

                    if (mTxtNowPos != null)
                        mTxtNowPos.setText(pos2date(mCurrentPosition));

                    mSeekBar.setProgress(mCurrentPosition);
                }
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }

         /**
          * 초기화
          */
        void init() {
            mVideoDuration = mPlayer.getDuration();
            if(mTxtVideoDuration != null)
                mTxtVideoDuration.setText(pos2date(mVideoDuration));    //
                                                                        //
            mSeekBar.setMax(mVideoDuration);                            // 동영상의 전체 재생 시간 설정
            mSeekBar.setOnSeekBarChangeListener(mSeekbarListener);      // 리스너 설정

            // 스케줄러. 200ms 이후부터 200ms간격으로 동작합니다.
            ScheduledExecutorService sv = Executors.newScheduledThreadPool(1);
            mFuture = sv.scheduleWithFixedDelay(new Runnable(){
                @Override
                public void run() {
                    mMoniteringHandler.sendEmptyMessage(0);
                }
            }, 200, 200, TimeUnit.MILLISECONDS);
        }

         /**
          * 스케줄러 릴리즈
          */
        public void release() {
            mFuture.cancel(true);
            Log.i("Player", "Release");
        }

         /**
          * timestamp 초시간을 실제 mm:ss 형태의 스트링으로 변환합니다.
          * @param pos timestamp
          * @return mm:ss 문자열
          */
        public String pos2date(int pos) {
            int conv = pos/1000;    // ms->s
            int min = conv/60;
            int sec = conv - (min * 60);

            String s = new String();
            if(min < 10)
                s += "0" + min;
            s += ":";
            if(sec < 10)
                s += "0" + min;

            return (min < 10 ? "0"+min : min) + ":" + (sec < 10 ? "0"+sec : sec);
        }
    }

}
