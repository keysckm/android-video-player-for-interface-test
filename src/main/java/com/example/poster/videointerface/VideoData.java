package com.example.poster.videointerface;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by KyeongMin on 2015-01-28.
 * 동영상 정보 모델
 */
public class VideoData implements Serializable{

    public long     VideoId;
    public String   VideoFilePath;
    public String   VideoTitle;
    public long     VideoSize;
    public long     VideoDuration;
    public String   VideoAddDate;
    public int      VideoWidth;
    public int      VideoHeight;
    public Bitmap   VideoThumbnail;

    public VideoData()
    {
        VideoId = 0;
        VideoFilePath = null;
        VideoTitle = null;
        VideoSize = 0;
        VideoDuration = 0;
        VideoAddDate = null;
        VideoWidth = 0;
        VideoHeight = 0;
        VideoThumbnail = null;
    }
}
