package com.example.poster.videointerface;

import android.content.Context;
import android.media.AudioManager;

/**
 * Created by KyeongMin on 2015-01-29.
 * 음량 관련 컨트롤
 */
public class SoundCtrl {

    /**
     * AudioManager 호출
     * @param c context
     * @return AudioManager 객체
     */
    private static AudioManager __getAudioManager(Context c) {
        return (AudioManager)c.getSystemService(Context.AUDIO_SERVICE);         // audio manager 호출
    }

    /**
     * 볼륨을 한단계 올립니다.
     * @param c context
     */
    public static void volumeUp(Context c) {
        AudioManager am = __getAudioManager(c);
        int current_vol = am.getStreamVolume(AudioManager.STREAM_MUSIC);            // 현재 볼륨 크기
        int max_vol = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);             // 최대 크기를 얻어옴(기기마다 다름)
        int targ_vol = current_vol + 1; // 크기 증가
        if(targ_vol >= max_vol)         // 최대값보다 크면 최대값으로 고정
            targ_vol = max_vol;
        // Media Volume 설정, FLAG_SHOW_UI 플래그를 줘서 볼륨조절시 UI가 출력되도록 설정
        am.setStreamVolume(AudioManager.STREAM_MUSIC, targ_vol, AudioManager.FLAG_SHOW_UI);
    }

    /**
     * 볼륨을 한단계 낮춥니다.
     * @param c context
     */
    public static void volumeDown(Context c) {
        AudioManager am = __getAudioManager(c);
        int current_vol = am.getStreamVolume(AudioManager.STREAM_MUSIC);
        int targ_vol = current_vol - 1;
        if(current_vol <= 0)
            targ_vol = 0;

        am.setStreamVolume(AudioManager.STREAM_MUSIC, targ_vol, AudioManager.FLAG_SHOW_UI);
    }
}
