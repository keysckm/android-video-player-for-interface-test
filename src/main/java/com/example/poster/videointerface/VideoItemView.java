package com.example.poster.videointerface;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by KyeongMin on 2015-01-28.
 * 동영상 리스트 아이템 뷰 클래스
 */
public class VideoItemView extends LinearLayout {

    private ImageView   mIcon = null;
    private TextView    mTitle = null;

    public VideoItemView(Context context) {
        super(context);

        // videolist_item layout 메모리에 올림
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.videolist_item, this, true);

        mIcon = (ImageView)findViewById(R.id.IconVideoImg);
        mTitle = (TextView)findViewById(R.id.txtVideoName);
    }

    public void setThumbnail(Bitmap thumbnail)
    {
        mIcon.setImageBitmap(thumbnail);
    }

    public void setTitle(String title)
    {
        mTitle.setText(title);
    }
}
