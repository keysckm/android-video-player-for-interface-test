package com.example.poster.videointerface;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.lang.reflect.Method;

/**
 * Created by KyeongMin.
 * 스크린에 관련된 처리를 하는 클래스
 */
public class ScreenCtrl {

    static class Point {
        public int x = 0;
        public int y = 0;

        public Point() { }
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    static class Size {
        public int width = 0;
        public int height = 0;

        public Size() { }
        public Size(int w, int h){
            width = w;
            height = h;
        }
    }

    /**
     * 두 좌표 사이의 직선거리를 계산합니다.
     * @param a 기준 좌표
     * @param b 상대 좌표
     * @return 두 좌표 사이의 직선거리
     */
    static int getDistance(android.graphics.Point a, android.graphics.Point b) {
        return (int)Math.hypot(a.x-b.x, a.y-b.y);
    }
    static int getDistance(ScreenCtrl.Point a, ScreenCtrl.Point b) {
        return (int)Math.hypot(a.x-b.x, a.y-b.y);
    }

    /**
     * 화면의 자동 밝기 상태를 설정한다.
     * @param c context
     * @param b true라면 설정, false라면 해제
     */
    static void setAutoBrightness(Context c, boolean b) {
        int mode = (b ? Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC : Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        Settings.System.putInt(c.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, mode);
    }

    /**
     * 화면의 자동 밝기 상태를 확인한다.
     * @param c context
     * @return 자동밝기 상태이면 true, 그렇지 않으면 false 리턴
     */
    static boolean isAutoBrightness(Context c) {
        int mode = 0;
        try {
            mode = Settings.System.getInt(c.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        return mode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
    }

    /**
     * Window 밝기를 얻는다.
     * @param w 밝기를 가져올 Window 객체(android.view.Window)
     * @return 밝기 값(0.0f ~ 1.0f)
     */
    static float getWindowBrightness(Window w) {
        return w.getAttributes().screenBrightness;
    }

    /**
     * 현재 스크린 밝기를 얻는다.
     * @param c context
     * @return 에러 발생시 -1, 그렇지 않으면 global screen brightness value(0 ~ 255)
     */
    static int getScreenBrightness(Context c) {
        try {
            return Settings.System.getInt(c.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Screen 밝기를 설정한다.
     * @param c view context
     * @param b 밝기 값(0 ~ 255)
     */
    static void setScreenBrightness(Context c, char b) {
        if(b > 255)
            b = 255;
        else if(b < 0)
            b = 0;

        Settings.System.putInt(c.getContentResolver(), "screen_brightness", b);
    }

    /**
     * Window 밝기를 설정한다.
     * @param w 밝기를 설정할 Window 객체(android.view.Window)
     * @param b 밝기 값(0.0f ~ 1.0f)
     */
    static void setWindowBrightness(Window w, float b) {
        if(b > 1.0f)
            b = 1.0f;
        else if(b < 0.0f)
            b = 0.0f;

        WindowManager.LayoutParams params = w.getAttributes();
        params.screenBrightness = b;
        w.setAttributes(params);
    }

    /**
     * 현재 View의 상태를 Bitmapr으로 저장한다.
     * @param view Bitmap객체로 저장할 view
     * @return view의 상태가 그려진 Bitmap 객체
     */
    static Bitmap View2Bitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        if(view instanceof SurfaceView) {
            SurfaceView v = (SurfaceView) view;
            v.setZOrderOnTop(true);
            v.draw(canvas);
            v.setZOrderOnTop(false);
            return bitmap;
        }
        else {
            view.draw(canvas);
            return bitmap;
        }
    }

    /**
     * 화면의 전체 해상도를 구한다.
     * @return 전체 해상도 크기를 담은 ScreenCtrl.Size
     */
    @SuppressWarnings("deprecation")
    static Size getRealScreenSize() {
        Display display = MainActivity.global_mainactivity.getWindowManager().getDefaultDisplay();
        int realWidth;
        int realHeight;

        if (Build.VERSION.SDK_INT >= 17){
            //new pleasant way to get real metrics
            DisplayMetrics realMetrics = new DisplayMetrics();
            display.getRealMetrics(realMetrics);
            realWidth = realMetrics.widthPixels;
            realHeight = realMetrics.heightPixels;

        } else if (Build.VERSION.SDK_INT >= 14) {
            //reflection for this weird in-between time
            try {
                Method mGetRawH = Display.class.getMethod("getRawHeight");
                Method mGetRawW = Display.class.getMethod("getRawWidth");
                realWidth = (Integer) mGetRawW.invoke(display);
                realHeight = (Integer) mGetRawH.invoke(display);
            } catch (Exception e) {
                //this may not be 100% accurate, but it's all we've got
                realWidth = display.getWidth();
                realHeight = display.getHeight();
                Log.e("Display Info", "Couldn't use reflection to get the real display metrics.");
            }

        } else {
            //This should be close, as lower API devices should not have window navigation bars
            realWidth = display.getWidth();
            realHeight = display.getHeight();
        }

        return new ScreenCtrl.Size(realWidth, realHeight);
    }

    /**
     * dp를 px로 변환
     * @param dp px로 변환할 dp
     * @return 변환된 px
     */
    static float dp2px(int dp) {
        DisplayMetrics metrics = MainActivity.global_mainactivity.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    /**
     * px를 dp로 변환
     * @param px dp로 변환할 px
     * @return 변환된 dp
     */
    public static float px2dp(float px){
        DisplayMetrics metrics = MainActivity.global_mainactivity.getResources().getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);
    }
}
